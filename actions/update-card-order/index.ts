"use server";


import {auth} from "@clerk/nextjs";
import {createSafeAction} from "@/lib/create-safe-action";
import {revalidatePath} from "next/cache";
import {db} from "@/lib/db";
import {InputType, ReturnType} from "./types";
import {UpdateCardOrder} from "@/actions/update-card-order/schema";

const handler = async (data: InputType): Promise<ReturnType> => {
  const {userId, orgId} = auth();

  if (!userId || !orgId) {
    return {
      error: "Unauthorized"
    }
  }

  const {items, boardId} = data;
  let updatedCards;

  try {
    const transaction = items.map((card) =>
      db.card.update({
        where: {
          id: card.id,
          list: {
            board: {
              orgId,
            },
          },
        },
        data: {
          order: card.order,
          listId: card.listId,
        },
      }),
    );

    const updatedCards = await db.$transaction(transaction);

  } catch
    (e) {
    return {
      error: "Failed to reorder cards"
    }
  }

  revalidatePath(`/board/${boardId}`);
  return {data: updatedCards};
}

export const updateCardOrder = createSafeAction(UpdateCardOrder, handler);