import {z} from "zod";
import {CreateBoard} from "@/actions/create-board/schema";
import {Board} from "@prisma/client";
import {ActionState} from "@/lib/create-safe-action";

export type InputType = z.infer<typeof CreateBoard>;
export type ReturnType = ActionState<InputType, Board>