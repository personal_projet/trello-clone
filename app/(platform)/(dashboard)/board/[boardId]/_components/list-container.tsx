"use client";

import {ListWithCards} from "@/types";
import {ListForm} from "@/app/(platform)/(dashboard)/board/[boardId]/_components/list-form";
import {useEffect, useState} from "react";
import {ListItem} from "@/app/(platform)/(dashboard)/board/[boardId]/_components/list-item";
import {DragDropContext, Droppable} from "@hello-pangea/dnd";
import {useAction} from "@/hooks/use-action";
import {updateListOrder} from "@/actions/update-list-order";
import {toast} from "sonner";
import {Simulate} from "react-dom/test-utils";
import error = Simulate.error;
import {updateCardOrder} from "@/actions/update-card-order";

interface ListContainerProps {
  data: ListWithCards[];
  boardId: string;
}

function reorder<T>(list: T[], startIndex: number, endIndex: number) {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

export const ListContainer = ({data, boardId}: ListContainerProps) => {
  const {execute: executeUpdateListOrder} = useAction(updateListOrder, {
    onSuccess: () => {
      toast.success("List order updated");
    },
    onError: (error) => {
      toast.error(error);
    }
  });


  const {execute: executeUpdateCardOrder} = useAction(updateCardOrder, {
    onSuccess: () => {
      toast.success("Card order updated");
    },
    onError: (error) => {
      toast.error(error);
    }
  });

  const [orderData, setOrderData] = useState(data);

  useEffect(() => {
    setOrderData(data);
  }, [data]);

  const onDragEnd = (result: any) => {
    const {destination, source, type} = result;

    if (!destination) {
      return;
    }

    // Dropped in the same position
    if (
      destination.droppableId === source.droppableId &&
      destination.index === source.index
    ) {
      return;
    }

    // User moves a list
    if (type === "list") {
      const items = reorder(
        orderData,
        source.index,
        destination.index
      ).map((list, index) => ({...list, order: index}));

      setOrderData(items);
      executeUpdateListOrder({items, boardId});
    }

    if (type === "card") {
      let newOrderData = [...orderData];

      const sourceList = newOrderData.find(list => list.id === source.droppableId);
      const destList = newOrderData.find(list => list.id === destination.droppableId);

      if (!sourceList || !destList) {
        return;
      }

      // Check if cards exist on the source list
      if (!sourceList.cards) {
        sourceList.cards = [];
      }

      // Check if cards exist on the destination list
      if (!destList.cards) {
        destList.cards = [];
      }

      // Moving the card in the same list
      if (source.droppableId === destination.droppableId) {
        const reorderedCards = reorder(
          sourceList.cards,
          source.index,
          destination.index
        );

        reorderedCards.forEach((card, index) => {
          card.order = index;
        });

        sourceList.cards = reorderedCards;

        setOrderData(newOrderData);
        executeUpdateCardOrder({
          boardId: boardId,
          items: reorderedCards,
        });
        // User moves a card to another list
      } else {
        // Remove card from source list
        const [removedCard] = sourceList.cards.splice(source.index, 1);

        // Assign the card to the destination list
        removedCard.listId = destination.droppableId;

        // Add card to destination list
        destList.cards.splice(destination.index, 0, removedCard);

        sourceList.cards.forEach((card, index) => {
          card.order = index;
        });

        // Update the order of the cards in the destination list
        destList.cards.forEach((card, index) => {
          card.order = index;
        });

        setOrderData(newOrderData);
        executeUpdateCardOrder({
          boardId: boardId,
          items: destList.cards,
        });
      }
    }
  }

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Droppable droppableId="lists" type="list" direction="horizontal">
        {(provided) => (
          <ol
            {...provided.droppableProps}
            ref={provided.innerRef}
            className="flex gap-x-3 h-full"
          >
            {orderData.map((list, index) => (
              <ListItem
                key={list.id}
                index={index}
                data={list}
              />
            ))}
            {provided.placeholder}
            <ListForm/>
            <div className="flex-shrink-0 w-1"/>
          </ol>
        )}
      </Droppable>
    </DragDropContext>
  )
}