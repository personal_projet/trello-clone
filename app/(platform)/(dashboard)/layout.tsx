import {NavBar} from "@/app/(platform)/(dashboard)/_components/navbar";

interface DashboardLayoutProps {
  children: React.ReactNode

}

const DashboardLayout = ({ children } : DashboardLayoutProps) => {
  return (
    <div className="h-full">
      <NavBar/>
      {children}
    </div>
  );
};

export default DashboardLayout;