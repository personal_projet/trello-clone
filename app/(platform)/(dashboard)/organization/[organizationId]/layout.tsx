import {OrgControl} from "@/app/(platform)/(dashboard)/organization/[organizationId]/_components/org-control";
import {auth} from "@clerk/nextjs";
import {startCase} from "lodash";

interface OrganizationIdLayoutProps {
  children: React.ReactNode
}

export async function generateMetadata() {
  const {orgSlug} = auth();

  return {
    title: startCase(orgSlug || "Organization"),
  }

}
const OrganizationIdLayout = ({children}: OrganizationIdLayoutProps) => {
  return (
    <>
      <OrgControl />
      {children}
    </>
  );
}

export default OrganizationIdLayout;