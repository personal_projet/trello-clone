import {SideBar} from "@/app/(platform)/(dashboard)/_components/sidebar";

interface OrganizationLayoutProps {
  children: React.ReactNode
}

const OrganizationLayout = ({children}: OrganizationLayoutProps) => {
  return (
    <main className="pt-20 md:pt-25 px-4 max-w-6xl 2xl:max-w-screen-xl mx-auto">
      <div className="flex gap-x-7">
        <div className="w-64 shrink-0 hidden md:block">
          <SideBar />
        </div>
        {children}
      </div>
    </main>
  );
}

export default OrganizationLayout;