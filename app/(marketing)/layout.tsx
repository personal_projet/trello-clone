import {NavBar} from "@/app/(marketing)/_components/navbar";
import {Footer} from "@/app/(marketing)/_components/footer";

const MarketingLayout = ({ children } : {children : React.ReactNode}) => {
  return (
    <div className="h-full bg-sky-50">
      <NavBar/>
      <main className="pt-40 pb-20 bg-sky-50">
        {children}
      </main>
      <Footer/>
    </div>
  )
}

export default MarketingLayout