import {InfoIcon, Medal} from "lucide-react";
import {Button} from "@/components/ui/button";
import localFont from "next/font/local";
import {Poppins} from "next/font/google";
import Link from "next/link";
import {cn} from "@/lib/utils";
import {Alert, AlertDescription, AlertTitle} from "@/components/ui/alert";

const headingFont = localFont({src: "/../../public/fonts/font.woff2"});

const textFont = Poppins({
  subsets: ["latin"],
  weight: ["100", "200", "300", "400", "500", "600", "700", "800", "900"],
});

export default function MarketingPage() {
  return (
    <div className="flex items-center justify-center flex-col">
      <div className={cn(
        "flex items-center justify-center flex-col",
        headingFont.className,
      )}>
        <div className="mb-4 flex items-center border shadow-sm p-4 bg-amber-200 text-amber-700 rounded-full">
          <Medal className="h-6 w-6 mr-2"/>
          Gestion des tâches numéro 1
        </div>
        <h1 className="text-3xl md:text-6xl text-center text-neutral-800 mb-6">
          Kanbano aide les équipes à avancer
        </h1>
        <div
          className="text-3xl md:text-4xl bg-sky-950 text-white px-4 p-2 rounded-md pb-4 w-fit">
          progresser 🐸
        </div>
      </div>
      <div className={cn(
        "text-sm md:text-xl text-neutral-500 mt-4 max-w-xs md:max-w-2xl text-center mx-auto",
        textFont.className,
      )}>
        Collaborez, gérez des projets et atteignez de nouveaux sommets de productivité.
        Du bureau en centre-ville au bureau à domicile, la façon dont votre équipe travaille est unique -
        accomplissez tout cela avec Kanbano.
      </div>
      <Button className="mt-6" size="lg" asChild variant="gradient">
        <Link href="/sign-up">
          Essayer Kanbano maintenant
        </Link>
      </Button>
      <Alert className="w-1/2 mt-24 bg-red-100">
        <InfoIcon/>
        <AlertTitle>Projet personnel - Aucun objectif commercial</AlertTitle>
        <AlertDescription>
          Kanbano est un clone de Trello, réalisé dans le cadre d&apos;un projet personnel.
          Il est possible de le tester mais immpossible de passer à la version Pro, Stripe est en mode test et refuse tous les paiements.
        </AlertDescription>
      </Alert>
    </div>
  )
}