# Clone de Trello

Ce projet est un clone de Trello, aucun lien avec l'entreprise Trello n'est à faire.

## Prérequis

- Docker et Docker Compose doivent être installés sur votre système.
- [Optionnel] Liste d'autres dépendances ou outils nécessaires.

## Mise en Place

### Cloner le Répertoire

```bash
git clone [URL_DU_REPERTOIRE]
cd [NOM_DU_REPERTOIRE]
```

### Configuration des Variables d'Environnement
Copiez le fichier .env.example en un nouveau fichier .env et remplissez les valeurs nécessaires.

## Lancement en Développement
Pour lancer l'application en mode développement :


```bash
docker-compose up --build
```

### Accès à l'Application
L'application est accessible à l'adresse suivante : http://localhost:3000

## Lancement en Production
Pour lancer l'application en mode production :

```bash
docker-compose -f docker-compose.prod.yml up --build
```

Cette commande utilise une configuration spécifique pour la production, sans monter votre répertoire de code source dans le conteneur.

### Accès à l'Application
L'application sera accessible à http://localhost:3000.

